package com.example.mspr

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Pour redirige via le page Agent
        val buttonAgent: Button =  findViewById(R.id.button_agent);
        buttonAgent.setOnClickListener(View.OnClickListener {
            val newIntent= Intent(application,AgentActivity::class.java)
            // Pour le title de page Agent
            newIntent.putExtra("title",getString(R.string.txt_title_agent))
            startActivity(newIntent)
        })
    }
}