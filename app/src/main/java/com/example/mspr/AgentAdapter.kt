package com.example.mspr

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.core.content.ContextCompat.startActivity

class AgentAdapter (private val agents: ArrayList<Agent>): RecyclerView.Adapter<AgentAdapter.ViewHolder>() {
    class ViewHolder (view: View) : RecyclerView.ViewHolder(view){
        val buttonViewAgent = view.findViewById<Button>(R.id.buttonViewAgent)
    }
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // RecycleView pour page Agent
        val view: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.cell_agent_detail, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val agent = agents.get(position)
        holder.buttonViewAgent.text=agent.name

        holder.buttonViewAgent.setOnClickListener(View.OnClickListener {
            // il faut passer le nom, l'image et la liste de matériels d'agent
            val context = holder.buttonViewAgent.context
            val newIntent = Intent(context, AgentActivity::class.java)
            // passer Nom + image + Liste de matériels
            newIntent.putExtra("name",agent.name)
            newIntent.putExtra("img",agent.img)
            newIntent.putCharSequenceArrayListExtra("materiels",agent.material) // ---Array return
            context.startActivity(newIntent)
        })

}