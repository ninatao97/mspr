package com.example.mspr

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.json.JSONObject
import java.io.IOException
import okhttp3.*

class AgentActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_agent)
        showBtnBack()

        // For header of page : Agence Name
        intent.getStringExtra("title")?.let { setHeaderTitle(it) }

        // relate to AgentAdapter :
        val agents = arrayListOf<Agent>()
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerViewAgentsDetail)
        recyclerView.layoutManager = LinearLayoutManager(this)
        val agentAdapter = AgentAdapter(agents)
        recyclerView.adapter = agentAdapter

        val okHttpClient: OkHttpClient = OkHttpClient.Builder().build()
        val mRequestURL ="https://djemam.com/epsi/categories.json"
        val request = Request.Builder()
            .url(mRequestURL)
            .get()
            .cacheControl(CacheControl.FORCE_NETWORK)
            .build()
        okHttpClient.newCall(request).enqueue(object : Callback{
            override fun onFailure(call: Call, e: IOException) {
                TODO("Not yet implemented")
            }
            override fun onResponse(call: Call, response: Response) {
                val data = response.body?.string()
                if(data !=null){
                    val jsOb= JSONObject(data)
                    // à modifier selon Agent dans json
                    val jsArray =jsOb.getJSONArray("items")
                    for(i in 0 until jsArray.length()){
                        val jsAgent = jsArray.getJSONObject(i)
                        val name =jsAgent.optString("name","")
                        val img =jsAgent.optString("img","")
                        val material =jsAgent.optString("material","")
                        val agent = Agent(name=name, img = img, material = material)
                        agents.add(agent)
                        Log.d("Agent",agent.name)


                    }
                    runOnUiThread(Runnable {
                        agentAdapter.notifyDataSetChanged()
                    })

                    Log.d("WS",data)
                    Log.d("agent","${agents.size}")
                }
            }
        })
    }


}